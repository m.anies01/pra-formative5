import java.util.logging.*;
import java.util.*;

public class LoggerTesting {
    public static void main(String[] args) {
        Logger log = Logger.getLogger(LoggerTesting.class.getName());
        
        try {
            Scanner inputNilai = new Scanner(System.in);
            String inputA,inputB;
            int a,b,hasil;

            System.out.print("Masukkan nilai [a] [b]: ");
            inputA = inputNilai.next();
            inputB = inputNilai.next();

            a = Integer.parseInt(inputA);
            b = Integer.parseInt(inputB);

            hasil = a/b;
            System.out.println("Hasil: "+hasil);

        } catch (NumberFormatException e){
            System.out.println("Error: "+e.toString());
            log.severe("Hello SEVERE "+log.getName());
        } finally {
            log.info("Hello INFO Logger "+log.getName());
        }
    }
}